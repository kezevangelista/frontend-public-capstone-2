// console.log("linked to the JS module")

//ADVANCE TASK ANSWER KEY:
let formSubmit = document.querySelector("#createCourse") 


formSubmit.addEventListener("submit", (event) => {
    event.preventDefault()//which is use to avoid automatic page redirection. 

   let name = document.querySelector("#courseName").value
  // console.log(name)
   // .value => describes the value attribute of the HTML element.
   let cost = document.querySelector("#coursePrice").value
   //console.log(cost)
   let desc = document.querySelector("#courseDescription").value 
  //console.log(desc)   

   //save the new entryb inside the database. by describing the request method/structure.

   fetch('https://secure-savannah-75849.herokuapp.com/courses/addCourse', {
   	   method: 'POST', 
   	   headers: {
   	   	 'Content-Type': 'application/json'
   	   },
   	   body: JSON.stringify({
   	   	  name: name,
   	   	  description: desc,
   	   	  price: cost 
   	   })
   })
   .then(res => {return res.json()
   }) //after describing the structure of the request body, now create the structure of the response coming from the back end.     
   .then(info => {
   	  if(info === true){
        alert('A New Course Is Successfully Created')
   	  }else {
        alert('Something Went Wrong When Adding New Course')
   	  }
   })
 }) 